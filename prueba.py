import math
import numpy as np
import string
import csv
import MySQLdb
import matplotlib.pyplot as plt
import json

def loadCSV(filename):
    matriz = []
    marcadores = 'RLOB','RSH','RELB','RWR','RGT','RKN','RAN','RHEE','RMT','LLOB',\
                 'LSH','LELB','LWR','LGT','LKN','LAN','LHEE','LMT'
    prueba = [[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],\
    [[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],[[],[],[]],\
    [[],[],[]],[[],[],[]],[[],[],[]]]
    with open(filename, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        count=1
        comienzo=0
        columna=[]
        tope='Trajectories'
        ch1 = [[],[],[]]
        ch2 = []
        ch3 = []
        for row in spamreader:
            if len(row)>0:
                linea=row[0]
                a=linea.split(" ")
                if (comienzo==0) and (tope in a):
                    comienzo=count+2
                else:
                    if count==comienzo:
                        for i in range(0,len(row)-1,1):
                            for m in range(len(marcadores)):
                                marcador = marcadores[m]
                                if marcador in row[i]:
                                    columna.append(i)
                    if (count>comienzo+2) and (columna != []):
                        for j in range(len(columna)):
                            prueba[j][0].append(float(row[columna[j]]))
                            prueba[j][1].append(float(row[columna[j]+1]))
                            prueba[j][2].append(float(row[columna[j]+2]))
                            # ch1.append(float(row[columna[j]]))
                            # ch2.append(float(row[columna[j]+1]))
                            # ch3.append(float(row[columna[j]+2]))

            count+=1
        print(prueba[1][0][1],prueba[1][1][1],prueba[1][2][1])
        # matriz.append((ch1,ch2,ch3))
    # return matriz

filname = './source/GP_W_065.csv'
loadCSV(filname)
