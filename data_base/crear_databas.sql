
CREATE SCHEMA `project_taller_3` ;

CREATE TABLE `project_taller_3`.`type_experiment` (
  `id_exp` INT NOT NULL,
  `type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_exp`));

CREATE TABLE `project_taller_3`.`speed` (
  `id_speed` INT NOT NULL,
  `value` FLOAT NOT NULL,
  PRIMARY KEY (`id_speed`));


CREATE TABLE `project_taller_3`.`experiments` (
  `id_exps` INT NOT NULL,
  `id_typexp` INT NOT NULL,
  `id_speed` INT NOT NULL,
  `id_subject` INT NOT NULL,
  PRIMARY KEY (`id_exps`, `id_typexp`, `id_speed`, `id_subject`));

CREATE TABLE `project_taller_3`.`subject` (
  `id_subject` INT NOT NULL,
  `name_subject` VARCHAR(45) NOT NULL,
  `description` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id_subject`));

----------------------------------------
CREATE TABLE `project_taller_3`.`markers` (
  `id_mark` INT NOT NULL,
  `name_mark` VARCHAR(45) NOT NULL,
  `description` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id_mark`));

CREATE TABLE `project_taller_3`.`values_markers` (
  `id_exp` INT NOT NULL,
  `id_mark` INT NOT NULL,
  `frq_sample` INT NOT NULL,
  `x` BLOB NOT NULL,
  `y` BLOB NOT NULL,
  `z` BLOB NOT NULL,
  PRIMARY KEY (`id_exp`, `id_mark`));

CREATE TABLE `project_taller_3`.`segments` (
  `id_segm` INT NOT NULL,
  `label` VARCHAR(10) NOT NULL,
  `id_prox` INT NOT NULL,
  `id_dist` INT NOT NULL,
  `coef` FLOAT NOT NULL,
  PRIMARY KEY (`id_segm`));

CREATE TABLE `project_taller_3`.`result` (
  `id_result` INT NOT NULL AUTO_INCREMENT,
  `id_exp` VARCHAR(45) NOT NULL,
  `region` VARCHAR(45) NOT NULL,
  `coef_correlation` FLOAT NOT NULL,
  PRIMARY KEY (`id_result`));
