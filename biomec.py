# -*- coding: utf-8 -*-
"""
Created on Thu Jun 14 23:35:32 2018

@author: lucia
"""

import string
import csv
import numpy as np
import matplotlib.pyplot as plt

with open('GP_W_065.csv', newline='') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
    count=1
    comienzo=0
    columna=0
    tope='Trajectories'
    x=['x','X']
    y=['y','Y']
    z=['z','Z']
    ch1=[]
    for row in spamreader:
#        print(row)
#        print(count)
        if len(row)>0:
            linea=row[0]
            a=linea.split(" ")
            if (comienzo==0) and (tope in a):
                comienzo=count+2
            else:
                if count==comienzo:
                    marcador=input("Indique el marcador con el que desea trabajar: ")
                    for i in range(0,len(row)-1,1):
                        if marcador in row[i]:
                            columna=i
                            eje=input("Indique el eje que desea graficar(X,Y,Z): ")
                                    
                if (count>comienzo+2) and (columna):
                    if eje in x:
                        ch1.append(float(row[columna]))
                    if eje in y:
                        ch1.append(float(row[columna+1]))
                    if eje in z:
                        ch1.append(float(row[columna+2]))
        count+=1
#        if count>10:
#            break
    t=np.arange(0,(len(ch1))*0.01,0.01)
    
    name=marcador+'.csv'
    myFile = open(name, 'w')
    with myFile:
        writer = csv.writer(myFile)
        writer.writerows(zip(t,ch1))