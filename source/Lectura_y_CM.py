#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import function as fc

class skeleton(object):

    def __init__(self,file,matriz,frec):
        # matriz = fc.loadCSV(self.filename)
        self.time = np.arange(0,(len(matriz[0][0]))*(1/frec),(1/frec))
        self.filename = file
        
        #Body
        self.BY = region([segment(RTH, marker(matriz[4]), marker(matriz[0]), 0.66),\
                            segment(LTH, marker(matriz[13]), marker(matriz[9]), 0.66)])
        #Rigth Arm
        self.RA = region([segment(RUA, marker(matriz[1]), marker(matriz[2]), 0.436),\
                            segment(RFAH, marker(matriz[2]), marker(matriz[3]), 0.682)])
        #Rigth Leg
        self.RL = region([segment(RT, marker(matriz[4]), marker(matriz[5]), 0.433),\
                            segment(RLG, marker(matriz[5]), marker(matriz[6]), 0.433),\
                            segment(RFOO, marker(matriz[6]), marker(matriz[8]), 0.5)])
        #Left Arm
        self.LA = region([segment(LUA, marker(matriz[10]), marker(matriz[11]), 0.436),\
                            segment(LFAH, marker(matriz[11]), marker(matriz[12]), 0.682)])
        #Left Leg
        self.LL = region([segment(LT, marker(matriz[13]), marker(matriz[14]), 0.433),\
                            segment(LLG, marker(matriz[14]), marker(matriz[15]), 0.433),\
                            segment(LFOO, marker(matriz[15]), marker(matriz[17]), 0.5)])

    def calculateCenterMass(self):
#        N_regions = 5
#        for t in range(len(self.time)):
#            centro_biz = self.brazo_iz.getCentroRegionTime(t)
#            centro_bd = rself.brazo_dr.getCentroRegionTime(t)
#            centro_piz = self.pierna_iz.getCentroRegionTime(t)
#            centro_pdr = self.pierna_dr.getCentroRegionTime(t)
#            centro_cu = self.cuerpo.getCentroRegionTime(t)
#            x = (centro_biz[0] + centro_bd[0] + centro_piz[0] + centro_pdr[0] + centro_cu[0])/N_regions
#            y = (centro_biz[1] + centro_bd[1] + centro_piz[1] + centro_pdr[1] + centro_cu[1])/N_regions
#            z = (centro_biz[2] + centro_bd[2] + centro_piz[2] + centro_pdr[2] + centro_cu[2])/N_regions
#            self.centro_eskeleton.append((x,y,z))


class region(object):

    def __init__(self, lista):
        self.segment_region = []
        for i in range(len(lista)-1):
            self.segment_region.append(lista[i])       
            
            
class segment(object):

    def __init__(self, name, m_prox, m_dist, coef):
        self.name = name
        self.list_segments = [m_prox , m_dist] #Id, punto1,punto2 que forman el segmento
        self.coef = coef
        self.CM = []

    def calculateCenterMass(self):
        C_dist = self.list_segment[0].getCoord()
        C_prox = self.list_segment[1].getCoord()
        self.CM.append = C_prox[0] + self.coef*(C_dist[0] - C_prox[0])
        self.CM.append = C_prox[1] + self.coef*(C_dist[1] - C_prox[1])
        self.CM.append = C_prox[2] + self.coef*(C_dist[2] - C_prox[2])
        
        
class marker(object):

    def __init__(self,arrayXYZ):
        self.x = arrayXYZ[0]
        self.y = arrayXYZ[1]
        self.z = arrayXYZ[2]
        
    def getCoord(self):
        return array(self.x), array(self.y), array(self.z)
        
    def getCoordTime(self,coordT):
        return self.x[coordT], self.y[coordT], self.z[coordT]

#%%

import sys
sys.path.insert(1, r'./../functions')  # add to pythonpath
from detect_peaks import detect_peaks

Esqueleto=initSkeleton(idexp)
Dtalind = detect_peaks(RHEE, valley=True)
Ltalind = detect_peaks(LHEE, valley=True)

