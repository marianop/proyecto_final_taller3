#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import string
import csv

def loadCSV(filename):
    marcadores = 'RLOB','RSH','RELB','RWR','RGT','RKN','RAN','RHEE','RMT','LLOB',\
                 'LSH','LELB','LWR','LGT','LKN','LAN','LHEE','LMT'
    for m in range(len(marcadores)):
        with open(filename, newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            count=1
            comienzo=0
            columna=0
            tope='Trajectories'
            ch1 = []
            ch2 = []
            ch3 = []
            marcador = marcadores[m]
            for row in spamreader:
                if len(row)>0:
                    linea=row[0]
                    a=linea.split(" ")
                    if (comienzo==0) and (tope in a):
                        comienzo=count+2
                    else:
                        if count==comienzo:
                            for i in range(0,len(row)-1,1):
                                if marcador in row[i]:
                                    columna=i
                        if (count>comienzo+2) and (columna):
                            ch1.append(float(row[columna]))
                            ch2.append(float(row[columna+1]))
                            ch3.append(float(row[columna+2]))

                count+=1
            print('CH1:',ch1[0],'CH2:',ch2[0],'CH3:',ch3[0],len(ch1))
        t = np.arange(0,(len(ch1))*0.01,0.01)
