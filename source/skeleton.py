#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import function as fc

class skeleton(object):

    def __init__(self,matriz,frec,coef):
        self.time = np.arange(0,(len(matriz[0][0]))*(1/frec),(1/frec))
        self.RHEE = marker(matriz[7])
        self.LHEE = marker(matriz[16])
        #Body
        self.TH = region([segment('RT', marker(matriz[4]), marker(matriz[1]), coef[0]),\
                            segment('LT', marker(matriz[13]), marker(matriz[10]), coef[6])])
        #Rigth Arm
        self.RA = region([segment('RUA', marker(matriz[1]), marker(matriz[2]), coef[1]),\
                            segment('RFAH', marker(matriz[2]), marker(matriz[3]), coef[2])])
        #Rigth Leg
        self.RL = region([segment('RT', marker(matriz[4]), marker(matriz[5]), coef[3]),\
                            segment('RLG', marker(matriz[5]), marker(matriz[6]), coef[4]),\
                            segment('RFOO', marker(matriz[6]), marker(matriz[8]), coef[5])])
        #Left Arm
        self.LA = region([segment('LUA', marker(matriz[10]), marker(matriz[11]), coef[7]),\
                            segment('LFAH', marker(matriz[11]), marker(matriz[12]), coef[8])])
        #Left Leg
        self.LL = region([segment('LT', marker(matriz[13]), marker(matriz[14]), coef[9]),\
                            segment('LLG', marker(matriz[14]), marker(matriz[15]), coef[10]),\
                            segment('LFOO', marker(matriz[15]), marker(matriz[17]), coef[11])])

        # ATRIBUTOS Y PROPIEDADES
        self.Angles_name=[]
        self.Angles = []
        self.MeanAngles = []
        self.StdAngles = []
        self.inicioVuelo_mean = []
        self.inicioVuelo_std = []
        # COEFICIENTE DE CORRELACION
        self.MeanCoefCorrelation = [] #Este tiene el coeficiente de correlacion del promedio de los angulos
        self.PasosCoefCorrelation = [] #Este tiene el coeficiente de correlacion de los angulos por cada ciclo
        self.MeanCoefCorrPasos = [] #Este tiene el el promedio de los coeficientes de arriba

    def returnMark(self,name):
        if self.TH.getSegment(name): return [self.TH.getSegment(name).prox,self.TH.getSegment(name).dist]
        if self.RA.getSegment(name): return [self.RA.getSegment(name).prox,self.RA.getSegment(name).dist]
        if self.RL.getSegment(name): return [self.RL.getSegment(name).prox,self.RL.getSegment(name).dist]
        if self.LA.getSegment(name): return [self.LA.getSegment(name).prox,self.LA.getSegment(name).dist]
        if self.LL.getSegment(name): return [self.LL.getSegment(name).prox,self.LL.getSegment(name).dist]

    def setAngles(self,list_name,lista):
        self.Angles_name=list_name
        self.Angles = lista

    def setDescriptores(self,mean,std):
        self.MeanAngles = mean
        self.StdAngles = std

    def setCoefCorrelation(self,lista1,lista2):
        self.MeanCoefCorrelation = lista1
        self.PasosCoefCorrelation = lista2
        self.MeanCoefCorrPasos = [np.mean(lista2[0]),np.mean(lista2[1]),np.mean(lista2[2])]

    def setTiempoVuelo(self,mean,std):
        self.inicioVuelo_mean = mean
        self.inicioVuelo_std = std

class region(object):

    def __init__(self, lista):
        self.segment_region = []
        for i in range(len(lista)):
            self.segment_region.append(lista[i])

    def getSegment(self, name):
        for seg in self.segment_region:
            if seg.name == name: return seg
        return None


class segment(object):
    def __init__(self, name, m_prox, m_dist, coef):
        self.name = name
        self.prox = m_prox
        self.dist = m_dist #Id, punto1,punto2 que forman el segmento
        self.coef = coef
        self.CM = []

    def calculateCenterMass(self):
        C_dist = self.list_segment[0].getCoord()
        C_prox = self.list_segment[1].getCoord()
        self.CM.append = C_prox[0] + self.coef*(C_dist[0] - C_prox[0])
        self.CM.append = C_prox[1] + self.coef*(C_dist[1] - C_prox[1])
        self.CM.append = C_prox[2] + self.coef*(C_dist[2] - C_prox[2])


class marker(object):

    def __init__(self,arrayXYZ):
        self.x = arrayXYZ[0]
        self.y = arrayXYZ[1]
        self.z = arrayXYZ[2]

    def getCoord(self):
        return array(self.x), array(self.y), array(self.z)

    def getCoordTime(self,coordT):
        return self.x[coordT], self.y[coordT], self.z[coordT]
