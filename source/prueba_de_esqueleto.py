# -*- coding: utf-8 -*-
"""
Created on Wed Aug  1 15:58:07 2018

@author: lucia
"""

import math
import numpy as np
import scipy.fftpack as fourier
import matplotlib.pyplot as plt
import function as fc #Libreria de funciones definidas por nosotros
import skeleton as sk #Objeto esqueleto, segmentos, puntos
import MySQLdb
import matplotlib.pyplot as plt
import json
import sys
sys.path.insert(1, r'./../functions')  # add to pythonpath
from detect_peaks import detect_peaks


#%%
coef=[0.66,0.436,0.682,0.433,0.433,0.5,0.66,0.436,0.682,0.433,0.433,0.5]
matriz=fc.loadCSV('GP_W_065.csv')
Esqueleto=sk.skeleton(matriz,100,coef)

fc.Angle(Esqueleto)
fc.calculateMeanStdAngle(Esqueleto)
fc.plotAngle(Esqueleto)
fc.calculateCoefCorrelation(Esqueleto)
