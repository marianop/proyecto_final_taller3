#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import scipy.fftpack as fourier
import matplotlib.pyplot as plt
import function as fc #Libreria de funciones definidas por nosotros
import skeleton as sk #Objeto esqueleto, segmentos, puntos
import MySQLdb
import matplotlib.pyplot as plt
import json
from tqdm import tqdm
from sklearn.cluster import KMeans

# Etapa 1:
# Creo la base de dato si no existe y cargo los datos, si existe no hace nada
#................cd ...............................................................
fc.createDataBase()

# x = fc.getIdExp()
id_exp = [21,22,23,24,81,82,83,84]#fc.getIdExp() #[81,82,83,84] #Defino que experimento quiero (esto de pureba luego cambiamos para que corra todo)
fs = 100
# Etapa 2:
# # Cargo de la base de datos los marcadores, y creo el esqueleto
# # ...............................................................................
for i in tqdm(range(len(id_exp))):
    #1 - Esqueleto
    #------------------------------------------
    Esqueleto = fc.initSkeleton(id_exp[i])

    #2 - Calculo de angulos
    #------------------------------------------
    fc.Angle(Esqueleto)

    #3 - Calcular angulos promedio y std y tiempo vuelo
    #------------------------------------------
    fc.calculateMeanStdAngle(Esqueleto)
    fc.tiempoVuelo(Esqueleto)

    #4 - Calcular coeficiente de correlacion
    #------------------------------------------
    fc.calculateCoefCorrelation(Esqueleto)

    #5 - Guardo en la base de datos los resultados
    #------------------------------------------
    fc.setResultDataBase(Esqueleto,id_exp[i])

    # fc.plotAngle(Esqueleto,id_exp[i])

# fc.clasificador(id_exp)

# print(Esqueleto.MeanCoefCorrPasos)
