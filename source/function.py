#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import math
import numpy as np
import string
import scipy.signal as sg
import csv
import MySQLdb
import matplotlib.pyplot as plt
import skeleton as sk
import json
import base64
import ast
from tqdm import tqdm
from sklearn.cluster import KMeans

import sys
sys.path.insert(1, r'./../functions')  # add to pythonpath
from detect_peaks import detect_peaks

n_infoUserDataBase = 0
n_infoSetDataBase  = 1

#===============================================================================
def loadDateJson(var):
    '''
        Esta funcion carga archivos .json y retorna un diccionario:

        name: loadDataJson()
        parameters:
            @var: tipo de configuracion:
                0: Carga informacion del host y la base de datos
                1: Carga todo los descriptores para ingresar a la base de datos
    '''
    if var == n_infoUserDataBase:
        with open('../infoConectionDB.json',encoding='utf-8') as data_file:
            data = json.load(data_file)
        return data
    elif var == n_infoSetDataBase:
        with open('../infoConectionDB0.json',encoding='utf-8') as data_file:
            data = json.load(data_file)
        return data
#===============================================================================
def createDataBase():
    '''
        Esta funcion se encarga de consultar si la base de datos existe y en caso de
        que no exista, crea el schema y todas las tablas correspondientes.

        name:createDataBase()
    '''
    # Connection parameters (aca llamamos a la funcion leerDatosDeTexto jaja falta)
    info_database = loadDateJson(n_infoUserDataBase)

    hostname = info_database['hostname']
    user_name = info_database['user_name']
    password = info_database['password']
    db_name = info_database['db_name']

    try:
        MySQLdb.connect(host=hostname, user=user_name, passwd=password, db=db_name, port=3306)
        print('\n\t-------------------------------------------------------------')
        print('\t\t[Base de datos existente, no fue necesario crearla]')
        print('\t-------------------------------------------------------------')
    except:
        print('\n\t-----------------------------------------------------------')
        print('\t\t[Se esta creando la base de datos, aguarde]')
        print('\t-------------------------------------------------------------')
        #create conection
        db = MySQLdb.connect(host=hostname, user=user_name, passwd=password,port=3306)
        # Prepare a cursor object using cursor() method
        cursor = db.cursor()
        # Below line  is hide your warning
        cursor.execute("CREATE SCHEMA " + db_name + ";")

        cursor.execute("CREATE TABLE" "`" + db_name + "`.`type_experiment` \
        (`id_exp` INT NOT NULL AUTO_INCREMENT,`type` VARCHAR(45) NOT NULL, PRIMARY KEY (`id_exp`));")

        cursor.execute("CREATE TABLE" "`" + db_name + "`.`subject` (\
              `id_subject` INT NOT NULL AUTO_INCREMENT,`name_subject` VARCHAR(45) NOT NULL,\
              `description` VARCHAR(60) NOT NULL, PRIMARY KEY (`id_subject`));")

        cursor.execute("CREATE TABLE" "`" + db_name + "`.`speed` \
        (`id_speed` INT NOT NULL AUTO_INCREMENT,`value` FLOAT NOT NULL, PRIMARY KEY (`id_speed`));")

        cursor.execute("CREATE TABLE" "`" + db_name + "`.`experiments` \
        (`id_exps` INT NOT NULL AUTO_INCREMENT,`id_typexp` INT NOT NULL,`id_speed` INT NOT NULL,`id_subject`\
         INT NOT NULL, PRIMARY KEY (`id_exps`, `id_typexp`, `id_speed`, `id_subject`));")

        cursor.execute("CREATE TABLE" "`" + db_name + "`.`markers` (\
                            `id_mark` INT NOT NULL AUTO_INCREMENT, `name_mark` VARCHAR(45) NOT NULL, \
                            `description` VARCHAR(100) NOT NULL, PRIMARY KEY (`id_mark`));")

        cursor.execute("CREATE TABLE" "`" + db_name + "`.`values_markers` (\
          `id_marks` INT NOT NULL AUTO_INCREMENT,`id_exp` INT NOT NULL,\
          `id_mark` INT NOT NULL,`x` LONGBLOB NOT NULL, `y` LONGBLOB NOT NULL,`z` LONGBLOB NOT NULL,\
          PRIMARY KEY (`id_marks`));")

        cursor.execute("CREATE TABLE" "`" + db_name + "`.`segments` (\
          `id_segm` INT NOT NULL AUTO_INCREMENT, `label` VARCHAR(10) NOT NULL,`id_prox` INT NOT NULL,\
          `id_dist` INT NOT NULL, `coef` FLOAT NOT NULL, PRIMARY KEY (`id_segm`));")

        cursor.execute("CREATE TABLE" "`" + db_name + "`.`result` (\
          `id_result` INT NOT NULL AUTO_INCREMENT,`id_exp` VARCHAR(45) NOT NULL,\
          `region` VARCHAR(45) NOT NULL,`coef_correlation` FLOAT NOT NULL, PRIMARY KEY (`id_result`));")

        cursor.execute("CREATE TABLE" "`" + db_name + "`.`descriptores` ( `id_exp` INT NOT NULL,\
            `descriptor` VARCHAR(45) NOT NULL, PRIMARY KEY (`id_exp`));")

        cursor.close()

        setDatesOfBaseD(hostname,user_name,password,db_name)

        print('\t-------------------------------------------------------------')
        print('\t\t [La base de datos fue creada con Exitos]')
        print('\t-------------------------------------------------------------')

#===============================================================================
#cursor,db_nameINSERT INTO `project_taller_3`.`speed`  VALUES (0,6);

def setDatesOfBaseD(hostname,user_name,password,db_name):
    '''
        Esta funcion carga los descriptores del json, y se encarga de ingresar
        tanto los descriptores como los valores a la base de datos.

        name: setDatesOfBaseD()
        parameters:
            @hostname: nombre del host
            @user_name: nombre de usuario del host
            @password: clave del usuario
            @db_name: nombre de la base de datos
    '''
    date_description = loadDateJson(n_infoSetDataBase)

    db = MySQLdb.connect(host=hostname, user=user_name, passwd=password, db=db_name ,port=3306)
    cursor = db.cursor()
    idlabel = {'id_exp':[],'id_speed':[],'id_subject':[],'id_exps':[],'id_for':[],'id_mark':[],'id_segm':[],'id_marks':[]}

    for tabla in tqdm(date_description):
        #.......................................................................
        if tabla == 'type_experiment':
            for i in range(len(date_description[tabla]['type'])):
                val2 = date_description[tabla]['type'][i]
                sql = "INSERT INTO "+tabla+"(type) VALUES('%s')" % \
                       (val2)
                try:
                    cursor.execute(sql) #Ejecuta los cambios en la base de datos
                    cursor.execute("SELECT LAST_INSERT_ID();")
                    idlabel['id_exp'].append(cursor.fetchall()[0][0])
                    db.commit() #Confirma los cambios en la base de datos
                except:
                   print("\nSe ha producido un error durante la ejecucion")
                   db.rollback() #deshace lo hecho
        #.......................................................................
        if tabla == 'speed':
            for i in range(len(date_description[tabla]['value'])):
                val2 = date_description[tabla]['value'][i]
                sql = "INSERT INTO "+tabla+"(value) VALUES('%i')" %\
                        (val2)
                try:
                    cursor.execute(sql)
                    cursor.execute("SELECT LAST_INSERT_ID();")
                    idlabel['id_speed'].append(cursor.fetchall()[0][0])
                    db.commit()
                except:
                   print("\nSe ha producido un error durante la ejecucion")
                   db.rollback()
        #.......................................................................
        if tabla == 'subject':
            for i in range(len(date_description[tabla]['name_subject'])):
                val2 = date_description[tabla]['name_subject'][i]
                val3 = date_description[tabla]['description'][i]
                sql = "INSERT INTO "+tabla+"(name_subject,description) VALUES('%s','%s')" %\
                        (val2,val3)
                try:
                    cursor.execute(sql)
                    cursor.execute("SELECT LAST_INSERT_ID();")
                    idlabel['id_subject'].append(cursor.fetchall()[0][0])
                    db.commit()
                except:
                   print("\nSe ha producido un error durante la ejecucion")
                   db.rollback()
        #.......................................................................
        if tabla == 'experiments':
            for sub in idlabel['id_subject']:
                for typ in idlabel['id_exp']:
                    if typ == 1:
                        for spe in idlabel['id_speed']:
                            sql = "INSERT INTO "+tabla+"(id_typexp,id_speed,id_subject) VALUES('%i','%i','%i')" %\
                                    (typ,spe,sub)
                            try:
                                cursor.execute(sql)
                                cursor.execute("SELECT LAST_INSERT_ID();")
                                data_ind = cursor.fetchall()[0][0]
                                idlabel['id_exps'].append(data_ind)
                                idlabel['id_for'].append(data_ind)
                                db.commit()
                            except:
                               print("\nSe ha producido un error durante la ejecucion")
                               db.rollback()
                    else:
                        for spe in range(len(idlabel['id_speed'])-1):
                            sql = "INSERT INTO "+tabla+"(id_typexp,id_speed,id_subject) VALUES('%i','%i','%i')" %\
                                    (typ,idlabel['id_speed'][spe],sub)
                            try:
                                cursor.execute(sql)
                                cursor.execute("SELECT LAST_INSERT_ID();")
                                idlabel['id_exps'].append(cursor.fetchall()[0][0])
                                db.commit()
                            except:
                               print("\nSe ha producido un error durante la ejecucion")
                               db.rollback()
        #.......................................................................
        if tabla == 'markers':
            for i in range(len(date_description[tabla]['name_mark'])):
                val2 = date_description[tabla]['name_mark'][i]
                val3 = date_description[tabla]['description'][i]
                sql = "INSERT INTO "+tabla+"(name_mark,description) VALUES('%s','%s')" %\
                        (val2,val3)
                try:
                    cursor.execute(sql)
                    cursor.execute("SELECT LAST_INSERT_ID();")
                    idlabel['id_mark'].append(cursor.fetchall()[0][0])
                    db.commit()
                except:
                   print("\nSe ha producido un error durante la ejecucion")
                   db.rollback()
        #.......................................................................
        if tabla == 'segments':
            for i in range(len(date_description[tabla]['label'])):
                val2 = date_description[tabla]['label'][i]
                val3 = date_description[tabla]['id_prox'][i]
                val4 = date_description[tabla]['id_dist'][i]
                val5 = date_description[tabla]['coef'][i]
                sql = "INSERT INTO "+tabla+"(label,id_prox,id_dist,coef) VALUES('%s','%i','%i','%f')" %\
                        (val2,val3,val4,float(val5))
                try:
                    cursor.execute(sql)
                    cursor.execute("SELECT LAST_INSERT_ID();")
                    idlabel['id_segm'].append(cursor.fetchall()[0][0])
                    db.commit()
                except:
                   print("\nSe ha producido un error durante la ejecucion")
                   db.rollback()
        #.......................................................................
        if tabla == 'values_markers':
            names = []
            for m in date_description['name_file']:
                for m2 in range(len(date_description['name_file'][m])):
                    names.append(date_description['name_file'][m][m2])
            init = 0
            fini = 1
            prueba = []
            rant = int(len(idlabel['id_for'])/4)
            for name in names:
                matrix = enCode(name)
                for val in idlabel['id_for'][init:fini]:
                    for mark in idlabel['id_mark']:#idlabel['id_mark']:
                        x = matrix[mark-1][0][0]
                        y = matrix[mark-1][1][0]
                        z = matrix[mark-1][2][0]
                        sql = "INSERT INTO "+tabla+"(id_exp,id_mark,x,y,z) VALUES('%i','%i','%s','%s','%s')" %\
                        (val,mark,x,y,z)
                        try:
                            cursor.execute(sql)
                            cursor.execute("SELECT LAST_INSERT_ID();")
                            idlabel['id_for'].append(cursor.fetchall()[0][0])
                            db.commit()
                        except:
                            print("\nSe ha producido un error durante la ejecucion")
                            db.rollback()

                init = fini
                fini = fini + 1

    cursor.close()
#===============================================================================
def loadCSV(filename):
    '''
        Esta funcion se encarga de parsear archivos csv las columnas con los
        marcadores definidos, retornando una matriz de array.

        name: loadCsv()
        parameters:
            @filname: ruta y nombre del archivo a parsear
    '''
    marcadores = 'RLOB','RSH','RELB','RWR','RGT','RKN','RAN','RHEE','RMT','LLOB',\
                 'LSH','LELB','LWR','LGT','LKN','LAN','LHEE','LMT'
    matriz = [[[],[],[]] for i in range(len(marcadores))]
    with open(filename, newline='') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        count=1
        comienzo=0
        columna=[]
        tope='Trajectories'
        ch1 = [[],[],[]]
        ch2 = []
        ch3 = []
        for row in spamreader:
            if len(row)>0:
                linea=row[0]
                a=linea.split(" ")
                if (comienzo==0) and (tope in a):
                    comienzo=count+2
                else:
                    if count==comienzo:
                        for i in range(0,len(row)-1,1):
                            for m in range(len(marcadores)):
                                marcador = marcadores[m]
                                if marcador in row[i]:
                                    columna.append(i)
                    if (count>comienzo+2) and (columna != []):
                        for j in range(len(columna)):
                            matriz[j][0].append(float(row[columna[j]]))
                            matriz[j][1].append(float(row[columna[j]+1]))
                            matriz[j][2].append(float(row[columna[j]+2]))

            count+=1
        return matriz

#===============================================================================
def enCode(filname):
    '''
        Esta funcion carga en una matriz, los archivos con los registros, y codifica
        en string una lista de valores float.

        name: enCode()
        parameters:
            @filname: nombre del archivo donde cagar la matriz y codificarla
    '''
    matrix = loadCSV(filname)
    encodMatrix = [[[],[],[]] for i in range(len(matrix))]
    for i in range(len(matrix)):
        for j in range(3):
            l3 = str(matrix[i][j])
            encodMatrix[i][j].append(l3)
    return encodMatrix

#===============================================================================
def deCode(list_cs):
    '''
        Esta funcion descodifica listas en formato de bytes, en listas de float.

        name: deCode()
        parameters:
            @list_cs: lista en formato bytes para decodificar.
    '''
    lis_i = str(list_cs)
    list_in = ast.literal_eval(lis_i[1:])
    salida = ast.literal_eval(list_in)
    return salida

#===============================================================================
def initSkeleton(id_exp):
    info_database = loadDateJson(n_infoUserDataBase)

    hostname = info_database['hostname']
    user_name = info_database['user_name']
    password = info_database['password']
    db_name = info_database['db_name']

    db = MySQLdb.connect(host=hostname, user=user_name, passwd=password, db=db_name, port=3306)
    cursor = db.cursor()

    frec = 100

    cursor.execute("SELECT x,y,z FROM values_markers where id_exp = %i" % id_exp)
    matrix_cod = cursor.fetchall()
    matrix_dec = []
    for i in range(len(matrix_cod)):
        x = matrix_cod[i][0]
        y = matrix_cod[i][1]
        z = matrix_cod[i][2]
        matrix_dec.append([deCode(x),deCode(y),deCode(z)])

    cursor.execute("SELECT coef FROM segments")
    coef = cursor.fetchall()
    cursor.close()
    Esqueleto = sk.skeleton(matrix_dec,frec,coef)
    return Esqueleto

#===============================================================================
def getIdExp():
    '''
        Esta funcion retorna una lista con los ID de experimentos, para luego
        generar los calculos para cada uno.

        name: getIdExp()
    '''
    info_database = loadDateJson(n_infoUserDataBase)

    hostname = info_database['hostname']
    user_name = info_database['user_name']
    password = info_database['password']
    db_name = info_database['db_name']

    db = MySQLdb.connect(host=hostname, user=user_name, passwd=password, db=db_name, port=3306)
    cursor = db.cursor()

    cursor.execute("SELECT DISTINCT id_exp FROM values_markers;")
    list_idx = cursor.fetchall()
    cursor.close()

    idx_notrepite = [list_idx[i][0] for i in range(len(list_idx))]
    # for i in range(1,len(list_idx)):
    #     if list_idx[i][0] != list_idx[i-1][0]:
    #         idx_notrepite.append(list_idx[i][0])
    #     if i <= len(list_idx):
    #         idx_notrepite.append(list_idx[i][0])

    return idx_notrepite
#===============================================================================
def setResultDataBase(skeleton,id_exp):
    info_database = loadDateJson(n_infoUserDataBase)

    hostname = info_database['hostname']
    user_name = info_database['user_name']
    password = info_database['password']
    db_name = info_database['db_name']
    tabla = 'result'

    db = MySQLdb.connect(host=hostname, user=user_name, passwd=password, db=db_name, port=3306)
    cursor = db.cursor()

    for i in range(len(skeleton.MeanCoefCorrPasos)):
        s = skeleton.Angles_name[i]+"-"+skeleton.Angles_name[i+3]
        sql = "INSERT INTO "+tabla+"(id_exp,region,coef_correlation) VALUES ('%i','%s','%f')" %\
                (id_exp,str(s),float(skeleton.MeanCoefCorrPasos[i]))
        try:
            cursor.execute(sql)
            db.commit()
        except:
           print("\nSe ha producido un error durante la ejecucion")
           db.rollback()
    cursor.close()
#===============================================================================
def detectaCiclos(skeleton):
    # Encontramos los puntos en que termina cada ciclo:
    Rtalind = detect_peaks(skeleton.RHEE.z, valley=True)
    Ltalind = detect_peaks(skeleton.LHEE.z, valley=True)

    Rdif=np.diff(np.array(Rtalind))
    Ldif=np.diff(np.array(Ltalind))


    Rmean = np.mean(Rdif)
    Lmean = np.mean(Ldif)

    Rtalind = detect_peaks(skeleton.RHEE.z,mpd = int(Rmean*0.75), valley=True)
    Ltalind = detect_peaks(skeleton.LHEE.z,mpd = int(Lmean*0.75), valley=True)

    Rdif=np.diff(np.array(Rtalind))
    Ldif=np.diff(np.array(Ltalind))

    Rmean = np.mean(Rdif)
    Lmean = np.mean(Ldif)

    Rtalind = detect_peaks(skeleton.RHEE.z,mpd = int(Rmean*0.75), valley=True)
    Ltalind = detect_peaks(skeleton.LHEE.z,mpd = int(Lmean*0.75), valley=True)

    return [Rtalind,Ltalind]
#===============================================================================

def muestreaCiclo(skeleton):

    talind=detectaCiclos(skeleton)
    Rtalind=talind[0]
    Ltalind=talind[1]

    fig1 = plt.figure()
    plt.plot(skeleton.RHEE.z)
    plt.title('Detección de minimos relativos')
    plt.plot(Rtalind,np.array(skeleton.RHEE.z)[np.array(Rtalind)],'ro')
    plt.figure()
    plt.plot(skeleton.LHEE.z)
    plt.plot(Ltalind,np.array(skeleton.LHEE.z)[np.array(Ltalind)],'ro')
    plt.show()

    markers=[[[],[]] for i in range(10)]
    aux=[[[],[]] for i in range(5)]
    for i in range(Rtalind[0]+1,Rtalind[len(Rtalind)-1]):
        aux[0][0].append(skeleton.TH.segment_region[0].dist.y[i])
        aux[0][1].append(skeleton.TH.segment_region[0].dist.z[i])

        aux[1][0].append(skeleton.TH.segment_region[0].prox.y[i])
        aux[1][1].append(skeleton.TH.segment_region[0].prox.z[i])

        aux[2][0].append(skeleton.RL.segment_region[0].dist.y[i])
        aux[2][1].append(skeleton.RL.segment_region[0].dist.z[i])

        aux[3][0].append(skeleton.RL.segment_region[1].dist.y[i])
        aux[3][1].append(skeleton.RL.segment_region[1].dist.z[i])

        aux[4][0].append(skeleton.RL.segment_region[2].dist.y[i])
        aux[4][1].append(skeleton.RL.segment_region[2].dist.z[i])

        if i in Rtalind:
            for j in range(5):
                markers[j][0].append(aux[j][0])
                markers[j][1].append(aux[j][1])
            aux=[[[],[]] for i in range(5)]

        #Izquierdo
    aux=[[[],[]] for i in range(5)]
    for i in range(Ltalind[0]+1,Ltalind[len(Ltalind)-1]):
        aux[0][0].append(skeleton.TH.segment_region[1].dist.y[i])
        aux[0][1].append(skeleton.TH.segment_region[1].dist.z[i])

        aux[1][0].append(skeleton.TH.segment_region[1].prox.y[i])
        aux[1][1].append(skeleton.TH.segment_region[1].prox.z[i])

        aux[2][0].append(skeleton.LL.segment_region[0].dist.y[i])
        aux[2][1].append(skeleton.LL.segment_region[0].dist.z[i])

        aux[3][0].append(skeleton.LL.segment_region[1].dist.y[i])
        aux[3][1].append(skeleton.LL.segment_region[1].dist.z[i])

        aux[4][0].append(skeleton.LL.segment_region[2].dist.y[i])
        aux[4][1].append(skeleton.LL.segment_region[2].dist.z[i])

        if i in Ltalind:
            for j in range(5):
                markers[j+5][0].append(aux[j][0])
                markers[j+5][1].append(aux[j][1])
            aux=[[[],[]] for i in range(5)]

    # print('Dif:',np.abs(len(markers[5][0])-len(markers[0][0])),'R',len(markers[0][0]),'L',len(markers[5][0]))

    # Igualamos la cantidad de ciclos en un lado y otro
    while len(markers[0][0])!=len(markers[5][0]):

        if len(markers[0][0])<len(markers[5][0]):
            for mark in range(5):
                for coord in [0,1]:
                    markers[mark+5][coord].pop()

        if len(Rtalind)>len(Ltalind):
            for mark in range(5):
                for coord in [0,1]:
                    markers[mark][coord].pop()

    markers2=[[[],[]] for i in range(10)]
    for i in range(len(markers2)):
        for coord in [0,1]:
            for paso in range(len(markers[0][0])):
                markers2[i][coord].append(sg.resample(markers[i][coord][paso],100))

    return markers2

#===============================================================================
def calcAngle(m1,m2,m3):
    ang=[]
    for paso in range(len(m1[0])):
        ang_pas=[]
        for i in range(len(m1[0][paso])):
            u=[(m1[0][paso][i]-m2[0][paso][i]),(m1[1][paso][i]-m2[1][paso][i])]
            v=[(m3[0][paso][i]-m2[0][paso][i]),(m3[1][paso][i]-m2[1][paso][i])]
            CosAng=(u[0]*v[0]+u[1]*v[1])/(np.linalg.norm(u)*np.linalg.norm(v))
            ang_pas.append(math.acos(CosAng)*180/math.pi) # Angulo en grados [-180,180]
        ang.append(ang_pas)
    return ang
#===============================================================================
def Angle(skeleton):
    marker = muestreaCiclo(skeleton)
    Angulos = [[],[],[],[],[],[]] # [RHip, RKnee, RAnkle, LHip, LKnee, LAnkle]
    for i in [0,1,2]:
        Angulos[i]=calcAngle(marker[i],marker[i+1],marker[i+2])
        Angulos[i+3]=calcAngle(marker[i+5],marker[i+6],marker[i+7])
    skeleton.setAngles(['RHip','RKnee','RAnkle','LHip','LKnee','LAnkle'],Angulos)
#===============================================================================
def calculateMeanStdAngle(skeleton):
    mean_angle = [[] for i in range(6)]
    std_angle = [[] for i in range(6)]
    for ang in range(len(skeleton.Angles)):
        mean_angle[ang] = np.mean(skeleton.Angles[ang],axis = 0)
        std_angle[ang] = np.std(skeleton.Angles[ang],axis = 0)

    skeleton.setDescriptores(mean_angle,std_angle)
#===============================================================================
def plotAngle(skeleton,idx):
    x = np.arange(0,100) #Porcentaje
    f = []
    g = []
    for i in range(len(skeleton.MeanAngles)):
        f.append(np.array(skeleton.MeanAngles[i])-np.array(skeleton.StdAngles[i]))
        g.append(np.array(skeleton.MeanAngles[i])+np.array(skeleton.StdAngles[i]))

    title=['Ángulo de cadera (derecha)','Ángulo de rodilla (derecha)','Ángulo de tobillo (derecha)',\
           'Ángulo de cadera (izquierda)','Ángulo de rodilla (izquierda)','Ángulo de tobillo (izquierda)']
    title2=['Ángulo de cadera','Ángulo de rodilla','Ángulo de tobillo']

    indice = [0,0,0,1,1,1]
    for i in range(3):
        fig1 = plt.figure()
        plt.subplot(2,1,1)
        plt.subplots_adjust(hspace=0.45)
        plt.title(title[i])
        plt.fill_between(x,f[i],g[i],alpha=0.3,edgecolor='None',facecolor='g')
        plt.plot(skeleton.MeanAngles[i],'b')
        plt.axvline(x=(skeleton.inicioVuelo_mean[indice[i]]-skeleton.inicioVuelo_std[indice[i]]),linestyle='--',color='k')
        plt.axvline(x=(skeleton.inicioVuelo_mean[indice[i]]),color='k')
        plt.axvline(x=(skeleton.inicioVuelo_mean[indice[i]]+skeleton.inicioVuelo_std[indice[i]]),linestyle='--',color='k')
        plt.xlabel('Índices')
        plt.ylabel('Grados (°)')
        plt.subplot(2,1,2)
        plt.title(title[i+3])
        plt.fill_between(x,f[i+3],g[i+3],alpha=0.3,edgecolor='None',facecolor='g')
        plt.plot(skeleton.MeanAngles[i+3],'b')
        plt.axvline(x=(skeleton.inicioVuelo_mean[indice[i+3]]-skeleton.inicioVuelo_std[indice[i]]),linestyle='--',color='k')
        plt.axvline(x=(skeleton.inicioVuelo_mean[indice[i+3]]),color='k')
        plt.axvline(x=(skeleton.inicioVuelo_mean[indice[i+3]]+skeleton.inicioVuelo_std[indice[i]]),linestyle='--',color='k')
        plt.xlabel('Índices')
        plt.ylabel('Grados (°)')
        fig1.savefig(str(idx)+str(i)+'.png')

        fig2 = plt.figure()
        plt.title(title2[i])
        plt.fill_between(x,f[i],g[i],alpha=0.3,edgecolor='None',facecolor='g')
        plt.plot(skeleton.MeanAngles[i],'b', label = 'Derecha')
        plt.fill_between(x,f[i+3],g[i+3],alpha=0.3,edgecolor='None',facecolor='m')
        plt.plot(skeleton.MeanAngles[i+3],'r',label = 'Izquierda')
        plt.axvline(x=(skeleton.inicioVuelo_mean[indice[i]]-skeleton.inicioVuelo_std[indice[i]]),linestyle='--',color='k')
        plt.axvline(x=(skeleton.inicioVuelo_mean[indice[i]]),color='k')
        plt.axvline(x=(skeleton.inicioVuelo_mean[indice[i]]+skeleton.inicioVuelo_std[indice[i]]),linestyle='--',color='k')
        plt.xlabel('Índices')
        plt.ylabel('Grados (°)')
        plt.legend()
        fig2.savefig(str(idx)+str(i)+'doble.png')
        # plt.show()

#===============================================================================
def tiempoVuelo(skeleton):
    marker = muestreaCiclo(skeleton)
    Indice = [[],[]]
    for j in range(len(marker[4][1])):
        Rmet = np.array(marker[4][1][j])
        Lmet = np.array(marker[9][1][j])

        Rumbral = np.min(Rmet)+20
        Lumbral = np.min(Lmet)+20

        Rind = 0
        i = 1
        while (i<len(Rmet)) and (Rind==0):
            if (Rmet[i-1]<Rmet[i]) and (Rmet[i]<=Rumbral) and (Rmet[i+1]>Rumbral):
                Rind = i
            i+=1

        Lind = 0
        i = 1
        while (i<len(Lmet)) and (Lind==0):
            if (Lmet[i-1]<Lmet[i]) and (Lmet[i]<=Lumbral) and (Lmet[i+1]>Lumbral):
                Lind = i
            i+=1

        Indice[0].append(Rind)
        Indice[1].append(Lind)

    Rmean = round(np.mean(Indice[0]))
    Lmean = round(np.mean(Indice[1]))

    Rstd = round(np.std(Indice[0]))
    Lstd = round(np.std(Indice[1]))

    skeleton.setTiempoVuelo([int(Rmean),int(Lmean)],[int(Rstd),int(Lstd)])


#===============================================================================
def calculateCoefCorrelation(skeleton):

    coef_corr1 = []
    u = []
    v = []
    coef_corr2 = [[],[],[]]
    coef_corr3 = [[],[],[]]
    for i in range(int(len(skeleton.MeanAngles)/2)):
        coef_corr1.append(np.corrcoef(skeleton.MeanAngles[i],skeleton.MeanAngles[i+3])[0][1])
        for j in range(len(skeleton.Angles[0])):
            coef_corr2[i].append(np.corrcoef(skeleton.Angles[i][j],skeleton.Angles[i+3][j])[0][1])

            u = np.array(skeleton.Angles[i][j])
            v = np.array(skeleton.Angles[i+3][j])
            coef_corr3[i].append(np.correlate(u,v)[0]/np.sqrt(np.correlate(u,u)[0]*np.correlate(v,v)[0]))
    skeleton.setCoefCorrelation(coef_corr1,coef_corr3)
#===============================================================================

def calculeDistance(p1,p2):
    dist = math.sqrt(np.abs((p2[0]-p1[0])**2)+np.abs((p2[1]-p1[1])**2))
    return dist

#===============================================================================
def setDescriptor(id_exp,id_reng):
    info_database = loadDateJson(n_infoUserDataBase)

    hostname = info_database['hostname']
    user_name = info_database['user_name']
    password = info_database['password']
    db_name = info_database['db_name']
    tabla = 'descriptores'

    db = MySQLdb.connect(host=hostname, user=user_name, passwd=password, db=db_name, port=3306)
    cursor = db.cursor()

    for i in range(len(id_exp)):
        if id_exp[i] in id_reng:
            label = 'Anormal'
        else:
            label = 'Normal'
        sql = "INSERT INTO "+tabla+"(id_exp,descriptor) VALUES('%i','%s')" % (id_exp[i],label)
        try:
            cursor.execute(sql)
            db.commit()
        except:
           print("\nSe ha producido un error durante la ejecucion")
           db.rollback()
    cursor.close()

#===============================================================================
def clasificador(id_exp):
    info_database = loadDateJson(n_infoUserDataBase)

    hostname = info_database['hostname']
    user_name = info_database['user_name']
    password = info_database['password']
    db_name = info_database['db_name']

    db = MySQLdb.connect(host=hostname, user=user_name, passwd=password, db=db_name, port=3306)
    cursor = db.cursor()

    #1 - Extraigo las velocidades
    #---------------------------------------------------------------------------
    cursor.execute("SELECT value FROM speed")
    data1 = cursor.fetchall()
    value_speed = [data1[i][0] for i in range(len(data1))]

    list_indx1 = []

    cursor.execute("SELECT id_exp FROM result")
    list_indx1.append(cursor.fetchall())
    db.commit()

    list_completa_idx = [int(list_indx1[0][i][0]) for i in range(len(list_indx1[0]))]

    #Extraigo vector con velocidades repetidas
    #...........................................................................
    speed = []
    for i in range(len(list_completa_idx)):
        sql = "SELECT value FROM speed INNER JOIN experiments on speed.id_speed = experiments.id_speed\
                WHERE experiments.id_exps = %i" % list_completa_idx[i]
        try:
            cursor.execute(sql)
            speed.append(float(cursor.fetchall()[0][0]))
            db.commit()
        except:
            print("\nSe ha producido un error durante la ejecucion")
            db.rollback()

    # #Extraigo coeficientes
    #...........................................................................
    idx_notrepite = []
    coef_list = []
    coef_comp = []
    #...........................................
    rodilla_coef = []
    cadera_coef = []
    tobillo_coef = []
    #...........................................
    speed_coef = []

    for i in range(len(id_exp)):
        sql = "SELECT coef_correlation FROM result WHERE id_exp = %i" % id_exp[i]
        try:
            cursor.execute(sql)
            db.commit()
            coef_list.append(cursor.fetchall())
            db.commit()
        except:
            print("\nSe ha producido un error durante la ejecucion")
            db.rollback()

    cursor.close()

    for i in range(len(coef_list)):
        cadera_coef.append(float(coef_list[i][0][0]))
        rodilla_coef.append(float(coef_list[i][1][0]))
        tobillo_coef.append(float(coef_list[i][2][0]))

    #completo velocidades por coef_comp
    for i in range(int(len(id_exp)/len(value_speed))):
        for j in range(len(value_speed)):
            speed_coef.append(value_speed[j])

    tobillo_rodilla = [[tobillo_coef[i],rodilla_coef[i]] for i in range(len(rodilla_coef))]
    cadera_rodilla = [[cadera_coef[i],rodilla_coef[i]] for i in range(len(rodilla_coef))]
    cadera_tobillo = [[cadera_coef[i],tobillo_coef[i]] for i in range(len(rodilla_coef))]

    km_t_r = KMeans(n_clusters=2, random_state=0, max_iter=300).fit(tobillo_rodilla)
    km_c_r = KMeans(n_clusters=2, random_state=0, max_iter=300).fit(cadera_rodilla)
    km_c_t = KMeans(n_clusters=2, random_state=0, max_iter=300).fit(cadera_tobillo)

    indx_rengo = []
    for i in range(len(tobillo_rodilla)):
        dist1 = calculeDistance(tobillo_rodilla[i],km_t_r.cluster_centers_[1])
        dist2 = calculeDistance(tobillo_rodilla[i],km_t_r.cluster_centers_[0])
        if dist1 < dist2:
            indx_rengo.append(i)

    # setDescriptor(id_exp,np.array(id_exp)[np.array(indx_rengo)])

    #==============================================================================================================================
    #...........................................................................
    plt.figure()
    ax = plt.gca()
    ax.cla() # clear things for fresh plot
    plt.plot(tobillo_coef,rodilla_coef,'*',label = 'Normales')
    plt.plot(np.array(tobillo_coef)[np.array(indx_rengo)],np.array(rodilla_coef)[np.array(indx_rengo)],'r*',label= 'Anormales')
    plt.plot(km_t_r.cluster_centers_[0][0],km_t_r.cluster_centers_[0][1],'ko')
    plt.plot(km_t_r.cluster_centers_[1][0],km_t_r.cluster_centers_[1][1],'ko')
    circle1 = plt.Circle((km_t_r.cluster_centers_[0][0],km_t_r.cluster_centers_[0][1]),0.001,color='r', alpha=0.2)
    circle2 = plt.Circle((km_t_r.cluster_centers_[1][0],km_t_r.cluster_centers_[1][1]),0.0025,color='b', alpha=0.2)
    ax.add_artist(circle1)
    ax.add_artist(circle2)
    plt.title('Asimetria Rodilla-Tobillo')
    plt.xlabel('A-ANKLE')
    plt.ylabel('A-KNEE')
    plt.legend()
    #...........................................................................
    plt.figure()
    ax = plt.gca()
    ax.cla() # clear things for fresh plot
    plt.plot(cadera_coef,rodilla_coef,'*',label = 'Normales')
    plt.plot(np.array(cadera_coef)[np.array(indx_rengo)],np.array(rodilla_coef)[np.array(indx_rengo)],'r*',label= 'Anormales')
    circle1 = plt.Circle((km_c_r.cluster_centers_[0][0],km_c_r.cluster_centers_[0][1]),0.002,color='r', alpha=0.2)
    circle2 = plt.Circle((km_c_r.cluster_centers_[1][0],km_c_r.cluster_centers_[1][1]),0.0025,color='b', alpha=0.2)
    plt.plot(km_c_r.cluster_centers_[0][0],km_c_r.cluster_centers_[0][1],'ko')
    plt.plot(km_c_r.cluster_centers_[1][0],km_c_r.cluster_centers_[1][1],'ko')
    ax.add_artist(circle1)
    ax.add_artist(circle2)
    plt.title('Asimetria Rodilla-Cadera')
    plt.xlabel('A-HIP')
    plt.ylabel('A-KNEE')
    plt.legend()
    #...........................................................................
    plt.figure()
    ax = plt.gca()
    ax.cla() #
    plt.plot(cadera_coef,tobillo_coef,'*',label = 'Normales')
    plt.plot(np.array(cadera_coef)[np.array(indx_rengo)],np.array(tobillo_coef)[np.array(indx_rengo)],'r*',label= 'Anormales')
    circle1 = plt.Circle((km_c_t.cluster_centers_[0][0],km_c_t.cluster_centers_[0][1]),0.002,color='r', alpha=0.2)
    circle2 = plt.Circle((km_c_t.cluster_centers_[1][0],km_c_t.cluster_centers_[1][1]),0.0015,color='b', alpha=0.2)
    plt.plot(km_c_t.cluster_centers_[0][0],km_c_t.cluster_centers_[0][1],'ko')
    plt.plot(km_c_t.cluster_centers_[1][0],km_c_t.cluster_centers_[1][1],'ko')
    plt.xlabel('A-HIP')
    plt.ylabel('A-ANKLE')
    plt.title('Asimetria Tobillo-Cadera')
    ax.add_artist(circle1)
    ax.add_artist(circle2)
    plt.legend()
    #==============================================================================================================================
    #...........................................................................
    plt.figure()
    plt.plot(speed_coef,cadera_coef,'o',label = 'Normales')
    plt.plot(np.array(speed_coef)[np.array(indx_rengo)],np.array(cadera_coef)[np.array(indx_rengo)],'ro',label= 'Anormales')
    plt.title('Asimetria Cadera-Velocidad')
    plt.xlabel('Velocidad')
    plt.ylabel('A-HIP')
    plt.legend()
    #...........................................................................
    plt.figure()
    plt.plot(speed_coef,rodilla_coef,'o',label = 'Normales')
    plt.plot(np.array(speed_coef)[np.array(indx_rengo)],np.array(rodilla_coef)[np.array(indx_rengo)],'ro',label= 'Anormales')
    plt.xlabel('Velocidad')
    plt.title('Asimetria Rodilla-Velocidad')
    plt.ylabel('A-KNEE')
    plt.legend()
    #...........................................................................
    plt.figure()
    plt.plot(speed_coef,tobillo_coef,'o',label = 'Normales')
    plt.plot(np.array(speed_coef)[np.array(indx_rengo)],np.array(tobillo_coef)[np.array(indx_rengo)],'ro',label= 'Anormales')
    plt.xlabel('Velocidad')
    plt.title('Asimetria Tobillo-Velocidad')
    plt.ylabel('A-ANKLE')
    plt.legend()
    plt.show()
